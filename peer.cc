#include <czmq.h>
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;

class NodeState {
//private:
  
public:

  zframe_t* My_Id;
  vector<zframe_t*> routing_table;
  int N;
  int GUID;

  NodeState()
      : GUID(0) {}
  
  bool ring_completed(int counter)
    {

        if (counter != 3)
          {
            return false; 
          }
        if (counter == 3)
          {
            return true; 
          }
    };
  
  void nodeJoin(zframe_t* Identity) {
  
    //assert(!ring_completed());
    zframe_t* id_frame = zframe_dup(Identity);
    routing_table.push_back(id_frame);
    free(Identity);
}

};


void sendDMsg(void* channel, vector<string> parts) {
  zmsg_t* msg = zmsg_new();
  for (const string& s : parts) {
    zmsg_addstr(msg, s.c_str());
  }
  zmsg_send(&msg, channel);
}



void sendRMsg(zsock_t* channel, zframe_t* to, vector<string> parts) {
  zmsg_t* msg = zmsg_new();
  zframe_t* dto = zframe_dup(to);
  zmsg_append(msg, &dto);
  for (const string& s : parts) {
    zmsg_addstr(msg, s.c_str());
  }
  zmsg_send(&msg, channel);
}

int handler(zloop_t*, zsock_t* channel, void* _node) {
  NodeState *node = reinterpret_cast<NodeState*>(_node);
  zmsg_t* msg = zmsg_recv(channel);
  zmsg_print(msg);

  zframe_t* identity = zmsg_pop(msg);
  zframe_t* active = zframe_dup(identity);
  zframe_t* action = zmsg_pop(msg);

  if (zframe_streq(action, "join")) {
    if (!node->ring_completed(node->N)) {
      
      
      cout<< node->N <<"HELLO BIACH YOU ARE GOING TO DE ADDED"<<endl;
      sendRMsg(channel, identity, {"HELLO BIACH YOU ARE GOING TO DE ADDED"});
      node->nodeJoin(identity);
      node->N++;

      if(node->ring_completed(node->N)){

          ////MULTICAST TO EVERY ONE THEIR CORRESPONDING TABLES.
          cout << "EVERYONE ITS LOGGED IN BIACH CREATING TABLES..."<<endl;

          ////REPARTIR TABLAS Y SERIALIZAR PARA ENVIAR.
          //+1 because i am adding my own ID
          int steps = (node->N / 2 ) + 1;

          cout<<"NUMBER OF REGISTERS PER TABLE: "<< steps <<endl;
          int counter = 0;
          for (auto o : node->routing_table)
          {
          	zmsg_t* table_to_send = zmsg_new();
            cout <<counter<<". "<< o << endl;
            cout<<"----------"<<endl;
	        for (int i = counter; i < steps+counter; ++i)
	          {
	          	
	          	int index = i%node->N;
	          	cout << index <<". "<<node->routing_table[index]<<endl;
	          	zmsg_push(table_to_send,zframe_dup(node->routing_table[index]));
	          	index = pow(2,i); 

	          }
	        cout<<"----------"<<endl;
	        sendRMsg(channel, o, {"YOUR UPDATES ARE GOING TO BE SENDED SHORTLY BITCH"});

	        zmsg_print(table_to_send);

          if(counter != 0)
            {
                zframe_t* dto = zframe_dup(o);
                zmsg_prepend(table_to_send, &dto);
                zmsg_send(&table_to_send, channel);    
            }
	        else if(counter == 0)
            {
                cout<<"THIS IS MY ROUTING TABLE BITCHES."<<endl;
                zmsg_print(table_to_send);
            }
            
            //zmsg_send(&table_to_send,channel);

          	counter++;
          }


        }
    } else {
      cout << "NO MORE PEERS SUPORRTED" << endl;
      sendRMsg(channel, identity, {"sorry we are full"});
    }
  }
}

int main(int argc, char** argv) {

  
  

 if (argc != 2) { ///arg se recibe por consola
    cout<< "Wrong execution!!" << endl << "need to put a parameter to start the network" << endl;
    return 1;
  }

  string myName(argv[1]);

  if(myName.compare("start") == 0){
    
    zctx_t* context = zctx_new();
    void* dealer = zsocket_new(context, ZMQ_DEALER);
    zsocket_connect(dealer, "tcp://localhost:5555");

    zsock_t *router = zsock_new(ZMQ_ROUTER);
    zsock_bind(router, "tcp://*:5555");

    sendDMsg(dealer, {"join"});

    NodeState *node = new NodeState();

    zloop_t *loop = zloop_new();
    zloop_reader(loop,router,&handler,node);
    zloop_start(loop);  
  
  }else
    {
      
      zctx_t* context = zctx_new();
    void* dealer = zsocket_new(context, ZMQ_DEALER);
    zsocket_connect(dealer, "tcp://localhost:5555");

    //zsock_t *router = zsock_new(ZMQ_ROUTER);
    //zsock_bind(router, "tcp://*:5555");

    sendDMsg(dealer, {"join"});
    
    zmsg_t* msg = zmsg_recv(dealer);
    zmsg_print(msg);

    cout << "ESPERANDO A Q TODOS LLEGEN PARA EL MENSAJE DE UPDATE "<< myName <<endl;

    zmsg_t* update = zmsg_recv(dealer);
   	zmsg_print(update);


    while(1)
      {
        cout << "recieved routing table" <<endl; 
			zmsg_t* msg = zmsg_recv(dealer);
   			zmsg_print(msg);        //
      }
    
    

    }


  //zsock_destroy(&router);
  return 0;
}












